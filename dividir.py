import sys
import pytest

def dividir(*args):
    quo = 0
    lista = list()
    try:
        if not args:
            #raise ValueError('Not parameters')
            raise ZeroDivisionError

        for i in range(len(args)):
            lista.append(float(args[i]))
            if i == 0:
                quo = lista[i]
            else:
                quo /= lista[i]
        return quo
        pass
    except ValueError:
        raise
    except ZeroDivisionError:
        raise
    except:
        print(sys.exc_info())

def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        dividir(1, 0)
    with pytest.raises(ZeroDivisionError):
        dividir('1', '0')
    with pytest.raises(ZeroDivisionError):
        dividir()

def test_num():
    assert dividir(1, 1) == 1
    assert dividir(3, 2) == 1.5
    assert dividir(8, 2, 2) == 2

    pass

def test_str():
    assert dividir('1', '1') == 1
    assert dividir('1', '2') == .5
    with pytest.raises(ValueError) as e:
        dividir('a', 1)
    e.match (r'could not convert string to float')
    assert dividir('8', '2', '2') == 2

if __name__ == '__main__':
    print(dividir(3, 2))
    print(dividir(3,2,2))
    pass